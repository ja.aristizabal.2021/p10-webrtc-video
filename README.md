# p10-webrtc-video

Repositorio de plantilla para la práctica 10 (WebRTC video) de PTAVI.

Recuerda que antes de empezar a trabajar con esta practica deberías realizar un
fork de este repositorio, con lo que crearás un nuevo repositorio en tu cuenta,
cuyos contenidos serán una copia de los de este.
Luego tendrás que clonar ese repositorio para tener una copia local con
la que podrás trabajar. No olvides luego hacer commits frecuentes, y
sincronizarlos con el servidor (`git push`) antes de la fecha de entrega
final de la práctica. Recuerda también que para que pueda ser corregido, el
repositorio deberá ser público a partir de la fecha de entrega.

[Enunciado de esta práctica](https://gitlab.com/cursomminet/code/-/blob/master/p10-webrtc-video/ejercicios.md).

__Nota (Ejercicio 7):__
El servidor *server_video_udp4* recibe con éxito en paralelo ofertas de otros clientes, 
además de la que ya está procesando. Sin embargo, *server_video_udp* no es capaz de
empezar a mandar el video a pesar de que el segundo cliente, supongamos *client_video_udp4(2)*,
recibe el mensaje answer. Error a tener en cuenta para la práctica final.