import asyncio
import json


register = {}     # Diccionario de clientes y servidores
registerServers = {}    # Diccionario que guarda los servidores
registerClients = {}    # Diccionario que guarda los clientes
countClient = 0         # Contador de clientes
partners = []           # Lista de servidores y clientes emparejasdos
serverToChoose = 0      # Variable que guarda el servidor a elegir
sendingAddr = None      # Variable que guarda la dirección del servidor a elegir


class EchoServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        serverName = message.split(':')     #  variable para extraer el nombre del servidor
        global countClient
        if message == "REGISTER CLIENT" or serverName[0] == "REGISTER SERVER":
            if message == "REGISTER CLIENT":
                registerClients[f'CLIENT({countClient})'] = addr
                countClient += 1
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
                print('New client registered: ', registerClients)
                chooseServer = json.dumps(registerServers)      #  litado de los servidores actuales
                self.transport.sendto(('Please, choose a Server: '+ chooseServer).encode(), addr)

            if serverName[0] == "REGISTER SERVER":
                registerMessage = 'OK'
                registerServers[serverName[1]] = addr
                self.transport.sendto(registerMessage.encode(), addr)
                print('New server registered: ', registerServers)





        else:
            global  sendingAddr
            if addr in registerClients.values():
                print("Message received from client")

                if message in registerServers.keys():
                    sendingAddr = registerServers[message]      # guardando la dirección del servidor
                    print(sendingAddr)

                else:
                    mensajeOferta = message
                    print(f"Send to ({sendingAddr}):", mensajeOferta)
                    register['CLIENT'] = addr
                    register['SERVER'] = sendingAddr
                    partners.extend([register])    # emparejando clientes y servidores
                    print(partners)
                    try:
                        self.transport.sendto(message.encode(), partners[-1]['SERVER'])
                    except:
                        print('Waiting for register server...')     # en el caso de no haber servidor


            else:
                mensajeAnswer = message
                print(f"Send to ({partners[-1]['CLIENT']}):  ", mensajeAnswer)
                self.transport.sendto(message.encode(), partners[-1]['CLIENT'])
                if message == '{"type": "bye"}':        #   cuando acaba la conexion entre cliente y servidor
                    register.clear()                        #   se vacia el diccionario
                    print(register)





async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 6789))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())