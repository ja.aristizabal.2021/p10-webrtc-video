import asyncio
import json


register={}     # Diccionario de clientes y servidores
addrServer = None

class ServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if message == "REGISTER CLIENT" or message == "REGISTER SERVER":
            if message == "REGISTER CLIENT":
                register['CLIENT'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
            if message == "REGISTER SERVER":
                register['SERVER'] = addr
                registerMessage = 'OK'
                global addrServer
                addrServer= addr
                self.transport.sendto(registerMessage.encode(), addr)
            print(register)
        else:
            if addr == register['CLIENT'] :
                mensajeOferta = message
                print(f"Send to ({register['SERVER']}): ", mensajeOferta)
                try:
                    self.transport.sendto(message.encode(), register['SERVER'])
                except:
                    print('Waiting for register server...')

                #self.transport.sendto('message'.encode(), register['SERVER'])

            else:
                mensajeAnswer = message
                print(f"Send to ({register['CLIENT']}):  ", mensajeAnswer)
                self.transport.sendto(message.encode(), register['CLIENT'])




async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ServerProtocol(),
        local_addr=('127.0.0.1', 6789))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())