import asyncio
import json


register={}         # Diccionario de clientes conectados con el servidor
registerClients={}  # Diccionario de clientes
count = 0           # Contador de clientes
class ServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        global count
        if message == "REGISTER CLIENT" or message == "REGISTER SERVER":
            if message == "REGISTER CLIENT":
                register['CLIENT'] = addr
                count += 1
                registerClients[f'CLIENT({count})'] = addr      # Agrega al diccionario de clientes
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
                print('New client registered: ', registerClients)

            if message == "REGISTER SERVER":
                register['SERVER'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
            print(register)
        else:
            if addr == register['CLIENT'] :
                mensajeOferta = message
                print(f"Send to ({register['SERVER']}): ", mensajeOferta)
                try:
                    self.transport.sendto(message.encode(), register['SERVER'])

                except:
                    print('Waiting for register server...')

            else:
                mensajeAnswer = message
                print(f"Send to ({register['CLIENT']}):  ", mensajeAnswer)
                self.transport.sendto(message.encode(), register['CLIENT'])




async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ServerProtocol(),
        local_addr=('127.0.0.1', 6789))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())